# How to Contribute

## New Users
If you're new to Git or never used it before, contributing might be confusing! If you're not interested in learning Git, please email me at kristenmarcinek@protonmail.com and I can add the resources and information you provide. If you are interested in learning Git to contribute to this or other projects, there are many great tutorials provided through Gitlab, Github, and on Youtube.

## Familiar with Git?
Contributing to this is pretty open-ended. As long as the information contributed is relevant and the format is similar, I will merge to master. If you find any information that is wrong or otherwise problematic, please feel free to raise an issue or pull and change it!