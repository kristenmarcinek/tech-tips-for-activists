# Tech Tips for Activists
This is a repository for those who are attending protests or who are interested in the intersection of cybersecurity and activism. Please make sure to read the changelog and contributing documents if you're interested in contributing! This is currently a WIP and more information and instruction will be added soon.
## Index
1. [General Safety Tips](https://gitlab.com/kristenmarcinek/tech-tips-for-activists/-/blob/master/README.md#general-safety-tips)
2. [Packages](https://gitlab.com/kristenmarcinek/tech-tips-for-activists/-/tree/master#packages)
3. [Communication & Messaging](https://gitlab.com/kristenmarcinek/tech-tips-for-activists/-/blob/master/README.md#communication-messaging)
4. [Email](https://gitlab.com/kristenmarcinek/tech-tips-for-activists/-/blob/master/README.md#email)
5. [Browsers](https://gitlab.com/kristenmarcinek/tech-tips-for-activists/-/blob/master/README.md#browsers)
6. [Browser Extensions](https://gitlab.com/kristenmarcinek/tech-tips-for-activists/-/blob/master/README.md#browser-extensions)
7. [Encryption](https://gitlab.com/kristenmarcinek/tech-tips-for-activists/-/blob/master/README.md#encryption)
8. [Social Networks](https://gitlab.com/kristenmarcinek/tech-tips-for-activists/-/blob/master/README.md#social-networks)
9. [VPNs and Other Network Safety](https://gitlab.com/kristenmarcinek/tech-tips-for-activists/-/blob/master/README.md#vpns-and-other-network-safety)
10. [Physical Tools](https://gitlab.com/kristenmarcinek/tech-tips-for-activists/-/blob/master/README.md#physical-tools)

## General Safety Tips
- Refrain from using your full name on social media if you can
    - If you are posting about the protests, this makes it harder for people to track information back to you
- Likewise, refrain from having public accounts if you can
    - Similar to above, people who you don't want to see your information can't see your information
- Turn off face and fingerprint login on your phone
    - Police CANNOT make you input your password, but they can make you unlock your phone through other, biometric means
- Leave electronic devices other than your phone elsewhere
    - Leave your laptop, digital camera, tablets, etc. at home or with someone your trust
- Turn off metadata in your camera app or remove metadata from your photos and videos
    - Metadata such as geolocation can allow a picture to trace you back to a certain place and time
    - [Instructions for Android](https://www.howtogeek.com/303410/how-to-prevent-android-from-geotagging-photos-with-your-location/)
    - [Instructions for iPhone](https://ios.gadgethacks.com/how-to/stop-your-iphone-photos-from-broadcasting-your-location-others-0190815/)
- Wear face coverings
    - Not only will this protect yourself and other people from COVID-19, face coverings will throw off facial recognition software 
- Refrain from taking pictures and videos of other protestors
    - If another protestor is not wearing sufficient coverings or has signage or clothes that can lead back to them, this can put them at risk
- Enable disk encryption if your phone allows
    - This makes it so that the information, data, and files on your phone are harder to tap into and harder for others to delete
    - [Instructions for Android](https://www.howtogeek.com/141953/how-to-encrypt-your-android-phone-and-why-you-might-want-to/)
    - [Instructions for iPhone](https://www.zdnet.com/article/how-to-turn-on-iphone-ipad-encryption-in-one-minute/)
- Remove permissions from unnecessary apps, log out of unnecessary apps, or completely delete them
    - One of the main permissions that should be turned off is location. This makes sure that no apps are tracking your location so you don't leave a trail
- Use a complex, not easy to guess password and two-factor authentication
    - Longer passwords and two-factor authentication provide greater security for devices and accounts
- Turn off Wifi, Bluetooth, Data, and NFC if possible (turn on airplane mode)
    - All of these services can be used to track an exact place and time of an event or where a person was
- Charge your phone prior 
    - Phones can be vital tools if used properly. If bringing one, make sure it's charged up!

## Packages
- [Disroot](https://disroot.org/en)
    - This service provides many tools for activists ranging from email to messaging to cloud storage. One of the most robust and my favorite option
- [RiseUp](https://riseup.net/)
    - Another service specifically for activists, RiseUp is less robust than Disroot but is definitely still worth checking out

## Communication & Messaging
- [Signal](https://www.signal.org/)
- [Telegram](https://telegram.org/)
- [XMPP](https://xmpp.org/)

## Email
- [ProtonMail](https://protonmail.com/)
- [CounterMail](https://countermail.com/)
- [HushMail](https://www.hushmail.com/)

## Browsers
- [Firefox](https://www.mozilla.org/en-US/exp/firefox/new/)
- [Pale Moon](https://www.palemoon.org/)

## Browser Extensions
- [uMatrix](https://github.com/gorhill/uMatrix)
- [WebRTC Control](https://mybrowseraddon.com/webrtc-control.html)
- [HTTPS Everywhere](https://www.eff.org/https-everywhere)
- [Decentraleyes](https://decentraleyes.org/)

## Encryption
- [VeraCrypt](https://www.veracrypt.fr/en/Home.html)

## Social Networks
- [Mastodon](https://mastodon.social/about)
- [Diaspora](https://diasporafoundation.org/)

## VPNs and Other Network Safety
- [OpenVPN](https://openvpn.net/)
- [Tor](https://www.torproject.org/)
- [PiHole](https://pi-hole.net/)

## Physical Tools
- [Premier Body Armor](https://premierbodyarmor.com/collections/backpack-panels)
- [RFID Blockers](https://www.gearhungry.com/best-rfid-wallets/)
- [Faraday bags](https://faradaybag.com/)
- [Power bank](https://www.anker.com/products/variant/powercore-fusion/A1621011) & [chafon cable](https://www.amazon.com/gp/product/B07KBXQFMN/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)